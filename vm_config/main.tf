resource "random_id" "instance_id" {
  byte_length = 8
}

provider "google" {
	credentials = file("geocitizencv-3f29b76c1fdb.json")
	project = "${var.project}"
	region = "${var.region}"
}

data "google_compute_address" "wtf" {
	count = "${var.node_count}"
	name = "geoaddress${count.index+1}"
}

resource "google_compute_instance" "default" {
	count = "${var.node_count}"
	name  = "ss-demo3-${count.index}"
	machine_type = "g1-small"
	zone = "${var.zone}"
	allow_stopping_for_update = true
	boot_disk {
		initialize_params {
			image = "centos-7-v20200403"
		}
	}

//  metadata_startup_script = "sudo yum update && sudo yum install httpd -y && echo '<!doctype html><html><body><h1>Hello from Terraform on Google Cloud!</h1></body></html>' | sudo tee /var/www/html/index.html"

	metadata = {
		ssh-keys = "new_bember:${file(var.public_key_path)}"
//		ssh-keys = "kuslui:${file(var.public_key_path2)}"
//		ssh-keys = "alex:${file(var.public_key_path3)}"
	}

	network_interface {
		network = "default"
			access_config {
      // Include this section to give the VM an external ip address
				nat_ip = "${data.google_compute_address.wtf["${count.index}"].address}"
		}
	}

  // Apply the firewall rule to allow external IPs to access this instance
  tags = ["http-server"]
}

resource "google_compute_firewall" "http-server" {
  name    = "default-allow-http"
  network = "default"

  allow {
    protocol = "tcp"
    ports    = ["80"]
  }

  // Allow traffic from everywhere to instances with an http-server tag
  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["http-server"]
}

output "ip" {
  value = google_compute_instance.default[*].network_interface.0.access_config.0.nat_ip
}

//data "google_client_openid_userinfo" "me" {
//}

//resource "google_os_login_ssh_public_key" "cache" {
//  user =  data.google_client_openid_userinfo.me.email
//  key = file("${var.public_key_path}")
//}

//output "internal_ipaddr_info" {
//  value = google_compute_address.default[*].internal_ip
//}
